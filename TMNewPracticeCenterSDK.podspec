 #
#  Be sure to run `pod spec lint TMSDK.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  s.name         = "TMNewPracticeCenterSDK"
  s.version      = "0.0.12"
  s.summary      = "TM 新时代文明实践中心"
  s.homepage     = "https://www.360tianma.com"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author       = { "renxukui" => "renxukui@360tianma.com" }
  s.platform     = :ios
  s.platform     = :ios, "9.0"
  s.source       = { :git => "https://gitee.com/tmgc/TMNewPracticeCenterSpec.git", :tag => s.version }
  s.source_files = 'TMFramework/TMNewPracticeCenterSDK.framework/Headers/*.{h}'
  s.ios.vendored_frameworks = 'TMFramework/TMNewPracticeCenterSDK.framework'
  s.resources = "TMFramework/TMNewPracticeCenter.bundle"
  s.requires_arc = true
  s.dependency "TMSDK"
  s.dependency "TMUserCenter"
  s.dependency "SDCycleScrollView"
  s.dependency "TMComponentKitSDK"
  s.dependency "TMNewsSDK"
  s.dependency "TMYunXiSDK" 
end
